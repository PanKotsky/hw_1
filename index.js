const http = require('http');

const defaultHost = '127.0.0.1';
const defaultPort = 3000;

const options = process.argv.reduce((acc, cur) => {
  if (cur.includes('--')) {
    const paramValue = cur.split('=', 1);
    acc[paramValue[0].replace('--', '')] = paramValue[1];
  }
  return acc;
}, {});

let requestCount = 0;

const server = http.createServer((req, res) => {
  // eslint-disable-next-line no-plusplus
  ++requestCount;
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify({
    message: 'Request handled successfully',
    requestCount,
  }));
});

server.listen(options.port ?? defaultPort, options.hostname ?? defaultHost, () => {
  // eslint-disable-next-line no-console
  console.log(`Server running at http://${options.hostname ?? defaultHost}:${options.port ?? defaultPort}/`);
});
